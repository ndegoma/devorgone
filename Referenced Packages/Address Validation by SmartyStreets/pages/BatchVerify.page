<apex:page controller="SmartyStreets1.BatchVerifyController" tabStyle="Batch_Verify__tab" applyBodyTag="false" docType="html-5.0">
    <html xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <head>
		<title>Batch Verify - SmartyStreets</title>
		<apex:stylesheet value="{!URLFOR($Resource.SmartyStreets1__SLDS0121, 'assets/styles/salesforce-lightning-design-system-vf.css')}" />
        
    	
    </head> 
<body>
<div id="main" class="slds">
    <script>
        Sfdc.onReady(function(){
        	getStatus();           
        });
    </script>
    
 	<apex:outputPanel id="batchPanel">
        
        <p styleclass="summary"><b>Address Verification Overview</b> </p>
        <br/>
        
    <apex:form id="form">
        <apex:actionFunction name="getStatus" action="{!getStatus}" reRender="form"/>
        
        <apex:pageMessages id="messages"/>
        <apex:outputPanel id="abortPanel" rendered="{!verifying}">
        	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        	<apex:commandButton id="abortBtn" value="Abort Verification" action="{!abortVerification}" reRender="form"/>
        	<br/><br/>
        </apex:outputPanel>
        
        <apex:outputPanel >
            <apex:outputLabel >Address type:</apex:outputLabel>
        	<apex:selectRadio value="{!verifyMode}" label="Address type" style="display:inline;">
            	<apex:selectOptions value="{!VerifyOptions}"></apex:selectOptions>
        	</apex:selectRadio>
        <apex:commandButton value="Change Modes" action="{!changeMode}"/> 
            <br/><br/>  
        </apex:outputPanel>  
   
        <apex:outputPanel id="newPanel" rendered="{!verifyMode == 'New'}">
        <apex:outputLabel id="topTableSummary" styleClass="summary"> 
            <b style="color:blue">Unsubmitted</b>: <b>{!numUnverified}</b> addresses have not yet been submitted for verification
    	</apex:outputLabel>&nbsp;
    	<apex:commandButton id="verifyNewBtn" value="Submit These" action="{!verifyNew}" disabled="{!verifying}"/>&nbsp;
        <apex:outputText id="verifyNewStatus" value="{!verNewStatus}"/>
        <br/><a id="topToggle">[click to hide]</a> (first 150)<br/>
        <div id="topDiv" class="tableDiv">
            <table id="topTable" class="addressData"></table>
        </div><br/> 
        </apex:outputPanel>
        
        <apex:outputPanel id="invalidPanel" rendered="{!verifyMode == 'Invalid'}">
        <apex:outputLabel id="middleTableSummary" styleClass="summary"> 
            <b style="color:red">Invalid</b>: <b>{!numInvalid}</b> addresses have been submitted and were found invalid
    	</apex:outputLabel>&nbsp;
    	<!-- <apex:commandButton id="verifyInvalidBtn" value="Re-submit These" action="{!verifyInvalid}" disabled="{!verifying}"/>&nbsp; -->
        <apex:outputText id="verifyInvalidStatus" value="{!verInvalidStatus}"/>
        <br/><a id="middleToggle">[click to hide]</a> (first 150)<br/>
        <div id="middleDiv" class="tableDiv">
            <table id="middleTable" class="addressData"></table>
        </div><br/>
        </apex:outputPanel>
        
        <apex:outputPanel id="validPanel" rendered="{!(verifyMode == 'Valid' || verifyMode == 'Valid-old')}">
        <apex:outputLabel id="bottomTableSummary" styleClass="summary"> 
            <b style="color:green">Valid</b>: <b>{!numClean}</b> addresses have been confirmed to be valid over {!ageThreshold} months ago
    	</apex:outputLabel>&nbsp;
    	<apex:commandButton id="verifyValidBtn" value="Re-submit These" action="{!verifyValid}" disabled="{!verifying}"/>&nbsp;
        <apex:outputText id="verifyValidStatus" value="{!verValidStatus}"/>
        <br/><a id="bottomToggle">[click to hide]</a> (first 150 by ID, sorted alphabetically)<br/>
        <div id="bottomDiv" class="tableDiv">
            <table id="bottomTable" class="addressData"></table>
        </div><br/>
        </apex:outputPanel>

<!--        
        <apex:actionPoller id="poller" action="{!getStatus}" interval="5" rendered="{!poll}"
              reRender="verifyAllStatus,verifyNewStatus,verifyInvalidStatus,verifyValidStatus,mainSummaryBreakdown"/>
-->  
        <apex:actionPoller id="poller" action="{!getStatus}" interval="5" rendered="{!poll}" reRender="form"/>
        <apex:actionPoller id="messagePoller" action="{!getStatus}" interval="5" rendered="{!verifying}"
                           reRender="messages,poller,messagePoller"/>
<!--    </apex:form> -->
    <br/>
        
    <apex:includeScript value="{!$Resource.SmartyStreets1__jQuery}"/>
    <script>
        //        $(window).load(function(){
        //        	getStatus();
//			$("[id$='verifyNewStatus']").html(loadingIcon);            
        //        });
                       
        var loadingIcon = '&nbsp&nbsp<img src=\"/img/loading32.gif\" '+
               ' alt=\"Verifying\" title=\"Verifying\" '+
               'border=\"0\" height=\"16\" width=\"16\"/> ';
        
    	if ("{!JSINHTMLENCODE(verifyMode)}" == "New")
            document.getElementById("topTable").innerHTML = "{!JSENCODE(unverifiedHTML)}";
    	if ("{!JSINHTMLENCODE(verifyMode)}" == "Invalid") 
            document.getElementById("middleTable").innerHTML = "{!JSENCODE(invalidHTML)}";
    	if ("{!JSINHTMLENCODE(verifyMode)}" == "Valid")
            document.getElementById("bottomTable").innerHTML = "{!JSENCODE(validHTML)}";
    
        //    	$("div.tableDiv").hide();
    	
    	$("#topToggle").click(function(){
            if ($("#topToggle").html() == "[click to show]")
            	$("#topToggle").html("[click to hide]");
            else $("#topToggle").html("[click to show]");
            $("#topDiv").slideToggle();
        });
    
    	$("#middleToggle").click(function(){
            if ($("#middleToggle").html() == "[click to show]")
            	$("#middleToggle").html("[click to hide]");
            else $("#middleToggle").html("[click to show]");
            $("#middleDiv").slideToggle();
        });
    
    	$("#bottomToggle").click(function(){
            if ($("#bottomToggle").html() == "[click to show]")
            	$("#bottomToggle").html("[click to hide]");
            else $("#bottomToggle").html("[click to show]");
            $("#bottomDiv").slideToggle();
        });
    
    	$("[id$='verifyAllBtn']").click(function(){
            //            $("[id$='verifyAllStatus']").html(" Verifying... (may take a few minutes)");
            $("[id$='verifyAllStatus']").html(loadingIcon);
        });
    
    	$("[id$='verifyNewBtn']").click(function(){
            //           $("[id$='verifyNewStatus']").html(" Verifying... (may take a few minutes)");
            $("[id$='verifyNewStatus']").html(loadingIcon);
        });
    
    	$("[id$='verifyInvalidBtn']").click(function(){
            //            $("[id$='verifyInvalidStatus']").html(" Verifying... (may take a few minutes)");
            $("[id$='verifyInvalidStatus']").html(loadingIcon);
        });
    
    	$("[id$='verifyValidBtn']").click(function(){
            //            $("[id$='verifyValidStatus']").html(" Verifying... (may take a few minutes)");
            $("[id$='verifyValidStatus']").html(loadingIcon);
        });
        
        $("[id$='abortBtn']").click(function(){
            $("[id$='abortBtn']").after(loadingIcon);
        });
    
    //    	console.log({!keysMissing});
    	if ({!keysMissing} == true){
            console.log("Keys are missing");
            $("[id$='mainSummary']").before(" <b style=\'color:red\'>Unable to verify. Auth ID and/or Auth Token not found</b><br/>");
        }
    
    function startPolling(){
        //        console.log("Started polling");
        $("#poller").attr("rendered","true");
    }
    </script>
    </apex:form>
    <style>
        .slds {
        	padding: 20px;
        }
    	.addressData, .street {
        	border: 1px solid black;       	
        }
        table.addressData {
        	border-collapse: collapse;
        	width: 95%;
        }
        .tableDiv {      	
        	height: 600px;
        	overflow-y: auto; 
        }
		.slds td {
        	padding-right:10px;
        	padding-bottom:5px;
        }
        .tableDiv td, .slds th {
        	padding-right:3px;
        	padding-left:3px;
        	padding-bottom:0px;
        }
        .summary {
        	font-size:12pt;
        }
    </style>
        </apex:outputPanel>
    </div>
	</body>
	</html> 
</apex:page>