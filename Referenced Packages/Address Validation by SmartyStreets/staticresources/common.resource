/* Functionality common for all record types for SmartyStreets address verification
 * @author "Neo" Pimentel
 * Copyright 2015-2016 SmartyStreets LLC
 */

// Global variables
var liveaddress;
var footDict = ss; // footnoteDictionary (easier to read)
var recordName;
var primeILEcell; // The id of the element containing the standard SF address field
var primeAddrId;
var primeSummaryStr = ""; // Primary summary in string form

var secondILEcell;
var secondAddrId;
var secondSummaryStr = ""; // Secondary summary in string form

var formId;
var reqIsFilled;

function onLoad(){
    // Hide the table row that holds the standard address data, and replace it with our fields
    var addressRow = (primeILEcell != "") ? $("#"+primeILEcell).parent() : $("#"+secondILEcell).parent(); 
    addressRow.hide();
    addressRow.after(addressForm);
    generateCopyLink(); // Make the link that allows user to copy address fields from Primary to Secondary
    
    $(".addrCell").css("border", "1px solid white"); // This makes the address fields feel more connected
    $(".detailList").wrap("<form></form>"); // Necessary for SmartyStreets jQuery plugin to work
    //addressRow.after("<tr><td><h1>Address Information</h1></td></tr>");
    
    var shouldAutoVerify = (location.pathname == "/apex/Account_new" || 
                            location.pathname == "/apex/Contact_new" ||
                            location.pathname == "/apex/Lead_new") ? true : false;
    
    var APItarget = "US";
    if (APImode == "All") APItarget = "International";
    else if (APImode == "International") APItarget = "US | International";
    
    modifyButtons(); // Swap default buttons for 'Save' and 'Cancel'
    initCountrySelector();
    populateFields(); // Load current (old) address data into the custom fields
     
    liveaddress = initLiveAddress(shouldAutoVerify, APItarget, interGeocodes); // This also maps the fields
    setupResponseHandler(liveaddress); // Set up function to handle response data fom SmartyStreets
    preAcceptVerified(liveaddress); // Pre-accept addresses that are already verified
    createMissingFields(); // If layout is missing a meta field, create it
}

function modifyButtons(){
    $(".btn:visible").hide();  // Hide irrelevant buttons 
    $(".btn[name='inlineEditSave']").show();
    $(".btn[name='inlineEditCancel']").show();
    
    // change save function so it copies address fields into the SF form before submitting
    sfdcPage.oldsave = sfdcPage.save;
    
    sfdcPage.save = function(id) {
        if (primeILEcell == "" && secondILEcell == "") return;   	                 	       
        
        copyMetaToSF();
        
        var convertedPrimeStreet = $("#primaryStreet").val().replace(/(, +)/g,"\r\n");
        var primaryCountryVal = $("#primaryCountrySelector").val();
        var primaryCityVal = $("#primaryCity").val();
        var primaryStateVal = $("#primaryState").val();
        var primaryZipVal = $("#primaryZip").val();
        
        if (secondBaseId != "") {
            var convertedSecondStreet = $("#secondaryStreet").val().replace(/(, +)/g,"\r\n");
            var secondaryCountryVal = $("#secondaryCountrySelector").val();
        	var secondaryCityVal = $("#secondaryCity").val();
        	var secondaryStateVal = $("#secondaryState").val();
        	var secondaryZipVal = $("#secondaryZip").val();
        }
        
        setTimeout(function(){
            console.log($(".pbError").css("display"));
            if ($(".pbError").css("display") != "none"){
                console.log("Error saving record");
                return;
            }                      
            
            if (secondBaseId == "") {
                // The extension will update the address and meta fields in the database 
                saveFields(primaryCountryVal, convertedPrimeStreet, primaryCityVal, primaryStateVal,
                           primaryZipVal, primeVerified, primeVerDate, primeSummaryStr, primeCounty);
            }
            else {                        
                // The extension will update the address and meta fields in the database 
                saveFields(primaryCountryVal, convertedPrimeStreet, primaryCityVal, primaryStateVal,
                           primaryZipVal, primeVerified, primeVerDate, primeSummaryStr, primeCounty, 
                           secondaryCountryVal, convertedSecondStreet, secondaryCityVal, secondaryStateVal,
                           secondaryZipVal, secondVerified, secondVerDate, secondSummaryStr, secondCounty);
            }
        },1000);
        
        sfdcPage.oldsave(id);
       
    }	
    
    // change cancel button so it sends the user back to the View Page from edit, or deletes new record
    if (recordName != "New Account" && recordName != "New Contact" && recordName != "New Lead")
        $(".btn[name='inlineEditCancel']").click(gotoView);
    else {
        sfdcPage.oldrevert= sfdcPage.revert;
        sfdcPage.revert = function(id) {
            try{
                deleteNew();
            }
            catch(err){
                gotoView();
                console.log(err);
            }
        }
    }
}
/*
function saveRecord() {   
    setTimeout(function(){
    if (($(".btn[name='edit']").is(":visible"))) // Save must have succeeded
        gotoView();
    else $(window).resize();    
    }, 500); 
    
    setTimeout(function(){
    if (($(".btn[name='edit']").is(":visible"))) // Try again in case of lag
        gotoView();
    else $(window).resize();    
    }, 1500);        
}
*/
function initCountrySelector() {
    $("#primaryCountrySelector, #secondaryCountrySelector").selectToAutocomplete(); // Replace dropdown with autocomplete
    $("#primaryCountrySelector").next().attr("id", "primaryCountry");
    $("#secondaryCountrySelector").next().attr("id", "secondaryCountry");
        
    $("#primaryCountrySelector").change(function(){
        $("#primaryCountry").val($("#primaryCountrySelector").val());
        $("#primaryCountry").change();
    });
    $("#secondaryCountrySelector").change(function(){
        $("#secondaryCountry").val($("#secondaryCountrySelector").val());
        $("#secondaryCountry").change();
    });
}

function initLiveAddress(shouldAutoVerify, APItarget, shouldGeocode) {
    var liveaddress = $.LiveAddress({
        key: key,
        debug: false,
        automap: false,
        autoVerify: shouldAutoVerify,
        xIncludeInvalid: true,
        agent: 'plugin:salesforce@'+version,
        waitForStreet: true,
        certifyMessage: "Click here to use this address anyway",
        geocode: shouldGeocode,
        target: APItarget,
        addresses: [{
            country: '#primaryCountry',
            address1: '#primaryStreet',
            locality: '#primaryCity',
            administrative_area: '#primaryState',
            postal_code: '#primaryZip' 
        },
        {
        	country: '#secondaryCountry',
        	address1: '#secondaryStreet',
            locality: '#secondaryCity',
            administrative_area: '#secondaryState',
            postal_code: '#secondaryZip'
        }]
    });
    
    var addresses = liveaddress.getMappedAddresses();
    if (addresses.length > 0)
        primeAddrId = addresses[0].id();
    if (addresses.length == 2)
    	secondAddrId = addresses[1].id();  
    
    $("#primaryCountry").attr("autocomplete", "off");
    $("#secondaryCountry").attr("autocomplete", "off");
    
    return liveaddress;
}    

function setupResponseHandler(liveaddress) {
    liveaddress.on("Completed", function(event, data, previousHandler) {
//        alert("Completed!");
            
        if (typeof data.response == 'undefined' || typeof data.address == 'undefined') return;
//        console.log("Address is defined!");
        
        // Determine which address was just verified
//        console.log("Address ID: "+data.address.id());
        var addrType = (data.address.id() == primeAddrId) ? "primary" : "secondary";
        
        if (data.response.chosen != null){
			var summaryHTML = createSummary(data.response, addrType);
            var county = data.response.chosen.metadata.county_name;
            if (county == null) county = "";
        	var latitude = data.response.chosen.metadata.latitude;
            var longitude = data.response.chosen.metadata.longitude;
            
            if (latitude != null && longitude != null)
//            console.log('Latitude: '+latitude+', Longitude: '+longitude);
                updateLocation(addrType, latitude, longitude);
            updateMetaFields(addrType, summaryHTML, county, false); // Update verification date, summary, etc.
        }
        
		previousHandler(event, data);
	});
    
    liveaddress.on("AddressChanged", function(event, data, previousHandler) {
        //console.log(event);
    	if (typeof data.address == 'undefined') return;
        // Determine which address was just verified
        var addrType = (data.address.id() == primeAddrId) ? "primary" : "secondary";
        var summary = "Not yet submitted for verification";
        
        if (addrType == "primary")
            primeSummaryStr = summary;
        else secondSummaryStr = summary;
        
        // Clear all meta fields for this address
        updateMetaFields(addrType, summary, "", true);
        
    	previousHandler(event, data);
    });
    
    liveaddress.on("AddressWasInvalid", function(event, data, previousHandler){
        var addrType = (data.address.id() == primeAddrId) ? "primary" : "secondary";
        var summaryId = (addrType == "primary") ? primeSummaryId : secondSummaryId;
        
        if (data.response.raw[0] != null){
            var summaryHTML = createSummary(data.response, addrType);
            $("#"+summaryId+"_ileinner").html(summaryHTML);
            updateLocation(addrType, null, null);
        }
        
        previousHandler(event, data);
    });
}

// Creates a summary based on footnotes in the response data. 
// Stores summary string in a global variable and returns an HTML version.
function createSummary(response, addrType) {
    //console.log("Creating summary...");
    
    //Get the footnote data from the response
    var analysis = response.raw[0].analysis;
    var footnotes = analysis.footnotes;
    var dpvFootnotes = analysis.dpv_footnotes;
    
    // International
    if (typeof dpvFootnotes == "undefined") {
        var status = analysis.verification_status;
        var precision = analysis.address_precision;
        var summary = "";
        if (status == "Verified")
        	summary = "* Address is valid\r\n* Precision - "+precision;                       
        else if (status == "Partial")
        	summary = "* Address is invalid\r\n* Parts of address are valid\r\n* Precision - "+precision;
        else if (status == null || status == "None")
        	summary = "* Address is invalid"; 
    }
    else // US
    	var summary = explainFootnotes(footnotes, dpvFootnotes); // Translate the footnotes into English
    
    if (addrType == "primary")
        primeSummaryStr = summary;
    else secondSummaryStr = summary;
    
    //var summaryHTML = summary.replace(/(?:\r\n|\r|\n)/g, '<br />');
    var summaryHTML = summary.replace(/\r\n/g, '<br>'); // convert string to HTML
    return summaryHTML;
}

// Translates response footnotes into English
// Based on html/resources/js/pages/demo.js
function explainFootnotes(footnotes, dpvFootnotes) {
    var notes = [];

	if (typeof footnotes !== 'undefined') {
//        console.log("footnotes: "+footnotes);
		var ft = footnotes.split('#');
		ft = ft.slice(0, ft.length - 1);
		for (var idx=0; idx < ft.length; idx++) {
			notes.push(footDict.footnotes[ft[idx]]);
		}
	}
    
    if (typeof dpvFootnotes !== 'undefined') {
		for (var i = 0; i < dpvFootnotes.length; i += 2) {
			notes.push(footDict.dpvFootnotes[dpvFootnotes.substring(i, i+2)]);
		}
	}
	
	var summary = "";
    for (var idx=0; idx < notes.length; idx++) {
        summary += "* " + notes[idx];
        if (idx < notes.length - 1)
            summary += "\r\n";
    }
  
	return summary;
}

// Fire a change event for each field in the secondary address
function signalSecondaryChange() {
    $("#secondaryCountry").change();
    $("#secondaryStreet").change();
    $("#secondaryCity").change();
    $("#secondaryState").change();
    $("#secondaryZip").change();
}

// Populates fields with the data about the address and its verification
function updateMetaFields(addrType, summary, county, clear) {
    var verifiedId = (addrType == "primary") ? primeVerifiedId : secondVerifiedId;
    var summaryId = (addrType == "primary") ? primeSummaryId : secondSummaryId;
    var verDateId = (addrType == "primary") ? primeVerDateId : secondVerDateId;
    var verDate2Id = (addrType == "primary") ? primeVerDate2Id : secondVerDate2Id;
    var countyId = (addrType == "primary") ? primeCountyId : secondCountyId;
    var shouldMark = clear ? false : true;
    var chkboxImage = clear ? "/img/checkbox_unchecked.gif" : "/img/checkbox_checked.gif";
    var todayRaw = new Date();
    var today = clear ? "---" : todayRaw.toLocaleDateString();
    var county = clear ? "" : county;
    
    $("#"+verifiedId+"_chkbox").attr("src", chkboxImage);
    
    $("#"+verDateId+"_ileinner").html(today);
    $("#"+verDate2Id+"_ileinner").html(today);
    
    $("#"+summaryId+"_ileinner").html(summary);
    
    $("#"+countyId+"_ileinner").html(county);
    
//    console.log("Summary: "+summary+" #"+summaryId+"_ileinner: "+$("#"+summaryId+"_ileinner").html()); 
}
/*
// Copy address fields into standard SF form 
function copyAddrToSF() {
    //alert("Copying Address to SF...");  
        
    // Copy Primary Address
    if (primeBaseId != ""){
        // Re-insert newlines into freeform addresses
        var convertedPrimeStreet = $("#primaryStreet").val().replace(/(, +)/g,"\r\n"); 
        
    	activateAddrDialog("primary");
    	$("#"+primeBaseId+"country").val($("#primaryCountrySelector").val());
		$("#"+primeBaseId+"street").val(convertedPrimeStreet);
    	$("#"+primeBaseId+"city").val($("#primaryCity").val());
    	$("#"+primeBaseId+"state").val($("#primaryState").val());
    	$("#"+primeBaseId+"zip").val($("#primaryZip").val());
    	sfdcPage.getDialogById('InlineEditDialog').hide();
    }
    
    // Copy Secondary Address
    if (secondBaseId != ""){
        // Re-insert newlines into freeform addresses
        var convertedSecondStreet = $("#secondaryStreet").val().replace(/(, +)/g,"\r\n");
        
    	activateAddrDialog("secondary");
    	$("#"+secondBaseId+"country").val($("#secondaryCountrySelector").val());
    	$("#"+secondBaseId+"street").val(convertedSecondStreet);
    	$("#"+secondBaseId+"city").val($("#secondaryCity").val());
    	$("#"+secondBaseId+"state").val($("#secondaryState").val());
    	$("#"+secondBaseId+"zip").val($("#secondaryZip").val());
    	sfdcPage.getDialogById('InlineEditDialog').hide();
    }
}
*/
// Copy meta fields into SF form
function copyMetaToSF() {
    // Copy Primary meta fields
 
//    console.log("double-click primary checkbox");
    var shouldMark = ($("#"+primeVerifiedId+"_chkbox").attr("src") == "/img/checkbox_checked.gif") ? true : false;
    if (exists(primeVerifiedId+"_ilecell")) 
        dblClickField(primeVerifiedId+"_ilecell");
    $("#"+primeVerifiedId).prop("checked", shouldMark);
    primeVerified = shouldMark ? "true" : "false";
    
//    console.log("double-click primary date");
    if (exists(primeVerDateId+"_ilecell"))
    	dblClickField(primeVerDateId+"_ilecell");
    if ($("#"+primeVerDateId+"_ileinner").html() != "&nbsp;"){
    	$("#"+primeVerDateId).val($("#"+primeVerDateId+"_ileinner").html());
    	primeVerDate = $("#"+primeVerDateId+"_ileinner").text();
	} else primeVerDate = "";
    
//    console.log("double-click primary summary");
    if (exists(primeSummaryId+"_ilecell"))
   		dblClickField(primeSummaryId+"_ilecell");
    if (primeSummaryStr != "")
    	$("#"+primeSummaryId).val(primeSummaryStr);
    closeDialog();
    
    if (exists(primeCountyId+"_ilecell"))
    	dblClickField(primeCountyId+"_ilecell");
    if ($("#"+primeCountyId+"_ileinner").html() != "&nbsp;"){
    	$("#"+primeCountyId).val($("#"+primeCountyId+"_ileinner").html());
    	primeCounty = $("#"+primeCountyId+"_ileinner").text();
	} else primeCounty = "";
    
    // Copy Secondary meta fields
    
//    console.log("double-click secondary checkbox");
    var shouldMark = ($("#"+secondVerifiedId+"_chkbox").attr("src") == "/img/checkbox_checked.gif") ? true : false;
    if (exists(secondVerifiedId+"_ilecell"))
   		dblClickField(secondVerifiedId+"_ilecell");
    $("#"+secondVerifiedId).prop("checked", shouldMark);
    secondVerified = shouldMark ? "true" : "false";
    
//    console.log("double-click secondary date");
    if (exists(secondVerDateId+"_ilecell"))
    	dblClickField(secondVerDateId+"_ilecell");
    if ($("#"+secondVerDateId+"_ileinner").html() != "&nbsp;"){
    	$("#"+secondVerDateId).val($("#"+secondVerDateId+"_ileinner").html());
    	secondVerDate = $("#"+secondVerDateId+"_ileinner").text();
    } else secondVerDate = "";
    
//    console.log("double-click secondary summary");
    if (exists(secondSummaryId+"_ilecell"))
    	dblClickField(secondSummaryId+"_ilecell");
    if (secondSummaryStr != "")
    	$("#"+secondSummaryId).val(secondSummaryStr);
    closeDialog();
    
    if (exists(secondCountyId+"_ilecell"))
    	dblClickField(secondCountyId+"_ilecell");
    if ($("#"+secondCountyId+"_ileinner").html() != "&nbsp;"){
    	$("#"+secondCountyId).val($("#"+secondCountyId+"_ileinner").html());
    	secondCounty = $("#"+secondCountyId+"_ileinner").text();
	} else secondCounty = "";
}

// Copy the Primary Address fields into the Secondary Address fields 
function copyPrimeToSecond() {
    $("#secondaryCountry").val($("#primaryCountry").val());
    $("#secondaryCountrySelector").val($("#primaryCountrySelector").val());
    $("#secondaryStreet").val($("#primaryStreet").val());
    $("#secondaryCity").val($("#primaryCity").val());
    $("#secondaryState").val($("#primaryState").val());
    $("#secondaryZip").val($("#primaryZip").val());
    
    signalSecondaryChange(); // Let liveaddress know these fields changed
    
    $("#"+secondVerifiedId+"_chkbox").attr("src", $("#"+primeVerifiedId+"_chkbox").attr("src"));
    $("#"+secondVerDateId+"_ileinner").html($("#"+primeVerDateId+"_ileinner").html());
    $("#"+secondSummaryId+"_ileinner").html($("#"+primeSummaryId+"_ileinner").html());
    $("#"+secondCountyId+"_ileinner").html($("#"+primeCountyId+"_ileinner").html());
    
//    console.log("primeSummaryStr: "+$("#"+primeSummaryId+"_ileinner").html());
     
    var tempSummaryStr = $("#"+primeSummaryId+"_ileinner").html().replace(/(?:\r\n|\r|\n)/g, "");
    secondSummaryStr = tempSummaryStr.replace(/<br>/g, "\r\n");
//    console.log("secondSummaryStr: "+secondSummaryStr);
    
    if (liveaddress.getMappedAddresses()[0].status() == "accepted")
        liveaddress.getMappedAddresses()[1].accept();
    
    updateLocation('copy',null,null);
}

// Make invisible fields for fields that are not included in the layout
function createMissingFields() {
//    console.log("Creating any missing fields");
//    $("#addressInfo").after("<div id='hiddenFields' style='visibility:hidden'></div>");
    
    if (!exists(primeVerifiedId+"_ilecell"))
        $("#addressInfo").after("<input id='"+primeVerifiedId+"' type='hidden' checked='"+primeVerified+"'>"+
        "<img src= '"+getChkboxSrc(primeVerified)+"' id='"+primeVerifiedId+"_chkbox' style='visibility:hidden' width='1%'>");
    if (!exists(primeVerDateId+"_ilecell")){
        $("#addressInfo").after("<input id='"+primeVerDateId+"' type='hidden'>"+
                               "<div id='"+primeVerDateId+"_ileinner' style='display: none;'></div>");
        $("#"+primeVerDateId+"_ileinner").text(primeVerDate);
    }
    if (!exists(primeSummaryId+"_ilecell")){
        $("#addressInfo").after("<input id='"+primeSummaryId+"' type='hidden'>"+
                               "<div id='"+primeSummaryId+"_ileinner' style='display: none;'></div>");
        $("#primeSummaryId_ileinner").html(primeSummary);
    }
    if (!exists(primeCountyId+"_ilecell"))
        $("#addressInfo").after("<input id='"+primeCountyId+"' type='hidden'>"+
                               "<div id='"+primeCountyId+"_ileinner' style='display: none;'></div>");
    
    if (!exists(secondVerifiedId+"_ilecell"))
        $("#addressInfo").after("<input id='"+secondVerifiedId+"' type='hidden' checked='"+secondVerified+"'>"+
        "<img src= '"+getChkboxSrc(secondVerified)+"' id='"+secondVerifiedId+"_chkbox' style='visibility:hidden' width='1%'>");
    if (!exists(secondVerDateId+"_ilecell")){
        $("#addressInfo").after("<input id='"+secondVerDateId+"' type='hidden'>"+
                               "<input id='"+secondVerDateId+"_ileinner' type='hidden'></input>");
        $("#"+secondVerDateId+"_ileinner").text(secondVerDate);
	}
//    console.log("secondVerDate: "+secondVerDate);
    if (!exists(secondSummaryId+"_ilecell"))
        $("#addressInfo").after("<input id='"+secondSummaryId+"' type='hidden'>"+
                               "<input id='"+secondSummaryId+"_ileinner' type='hidden'>");
    if (!exists(secondCountyId+"_ilecell"))
        $("#addressInfo").after("<input id='"+secondCountyId+"' type='hidden'>"+
                               "<input id='"+secondCountyId+"_ileinner' type='hidden'>");
// Geolocation fields------------------------------------------------------------------------------------------
/*
  	$("#addressInfo").after("<input id='primeLatitude' type='hidden'>"+
                               "<input id='primeLatitude_ileinner' type='hidden'>");    
    $("#addressInfo").after("<input id='primeLongitude' type='hidden'>"+
                               "<input id='primeLongitude_ileinner' type='hidden'>");
    $("#addressInfo").after("<input id='secondLatitude' type='hidden'>"+
                               "<input id='secondLatitude_ileinner' type='hidden'>");
    $("#addressInfo").after("<input id='secondLongitude' type='hidden'>"+
                               "<input id='secondLongitude_ileinner' type='hidden'>");
*/
}

// Checks if element exists
function exists(elementId) {
//    console.log(elementId+" length= "+$("#"+elementId).length);
    if ($("#"+elementId).length > 0)
        return true;
    else return false;
}

// Returns string of HTML for the appropriate src of the checkbox
function getChkboxSrc(fieldValue) {
//    console.log("verified? "+fieldValue);
    if (fieldValue == "true")
    	return "/img/checkbox_checked.gif";
    else return "/img/checkbox_unchecked.gif";
}

// Simulates a double-click on the element with id=fieldId
function dblClickField(fieldId){
    if (fieldId == "") return;
    var dblClickEvent = new MouseEvent('dblclick',{
                    'view': window,
                    'bubbles': true,
                    'cancelabel': true
                });
    try{
		$("#"+fieldId).get(0).dispatchEvent(dblClickEvent);
    }
    catch(err){
        alert("There was a problem. "+err.message);
        console.log("Error programmatially double-clicking "+fieldId+": "+err.message);
    }
}

function closeDialog(){
    try{
    	sfdcPage.getDialogById('InlineEditDialog').hide();
    }
    catch(err){
        console.log("Error programmatially closing a dialog box: "+err.message);
    }
}

// This generates the standard address fields in the DOM
function activateAddrDialog(addrType) {
    var fieldId = (addrType == "primary") ? primeILEcell : secondILEcell;
 	dblClickField(fieldId);   
}